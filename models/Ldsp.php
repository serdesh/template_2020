<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ldsp".
 *
 * @property int $id
 * @property string $name Название
 * @property double $cost Цена
 * @property string $thickness Толщина
 * @property int $sorting Сортировка
 * @property string $type Тип
 *
 * @property OrdersLdsp[] $ordersLdsps
 */
class Ldsp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldsp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost'], 'number'],
            [['sorting'], 'integer'],
            [['name', 'thickness', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'cost' => 'Цена',
            'thickness' => 'Толщина',
            'sorting' => 'Сортировка',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps()
    {
        return $this->hasMany(OrdersLdsp::className(), ['ldsp_id' => 'id']);
    }

    public static function getLdspList()
    {
        $list = Ldsp::find()->all();
        return ArrayHelper::map($list, 'id', 'name');
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->thickness = (int)$this->thickness; 

    }

    public static function getThickness()
    {
        $list = Ldsp::find()->where(['type' => 'ЛДСП'])->groupBy(['thickness'])->orderBy('cast(thickness as unsigned) asc')->all();
        return ArrayHelper::map($list, 'id', 'thickness');
    }

    public static function getOrgalit()
    {
        $list = Ldsp::find()->where(['type' => 'Оргалит'])->orderBy(['sorting' => SORT_DESC])->all();
        return ArrayHelper::map($list, 'id', 'name');
    }
}
