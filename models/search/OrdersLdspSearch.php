<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrdersLdsp;

/**
 * OrdersLdspSearch represents the model behind the search form about `app\models\OrdersLdsp`.
 */
class OrdersLdspSearch extends OrdersLdsp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'ldsp_id', 'count', 'edge_width_left_id', 'edge_width_right_id', 'edge_height_left_id', 'edge_height_right_id'], 'integer'],
            [['width', 'height'], 'number'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdersLdsp::find()->with(['ldsp','order']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'ldsp_id' => $this->ldsp_id,
            'width' => $this->width,
            'height' => $this->height,
            'count' => $this->count,
            'edge_width_left_id' => $this->edge_width_left_id,
            'edge_width_right_id' => $this->edge_width_right_id,
            'edge_height_left_id' => $this->edge_height_left_id,
            'edge_height_right_id' => $this->edge_height_right_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
