<?php

namespace app\models\constants;

use app\models\Users;


class UsersConstants extends Users {
    const ROLE_ADMIN = 1;
    const ROLE_MODERATOR = 2;
    const ROLE_USER = 3;

    public static function getString($status)
    {
        switch ($status){
            case self::ROLE_ADMIN: return 'Администратор';
            case self::ROLE_MODERATOR: return 'Модератор';
            case self::ROLE_USER: return 'Пользователь';
        }
        return 'Неизвестный';
    }

    public static function getArray()
    {
        return [
            self::ROLE_ADMIN => self::getString(self::ROLE_ADMIN),
            self::ROLE_MODERATOR => self::getString(self::ROLE_MODERATOR),
            self::ROLE_USER => self::getString(self::ROLE_USER),
        ];
    }

    public static function getImage($image)
    {
        if (!file_exists('uploads/avatars/'.$image) || $image == '') {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/img/nouser.png';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/avatars/'.$image;
        }
        return $path;
    }

}
