<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_additional".
 *
 * @property int $id
 * @property int $order_id Заказы
 * @property int $additional_id Доп
 * @property int $temporary_id Временно
 * @property int $count Количество
 *
 * @property Additional $additional
 * @property Orders $order
 * @property Temporaries $temporary
 */
class OrdersAdditional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_additional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'additional_id', 'temporary_id', 'count'], 'integer'],
            [['additional_id'], 'exist', 'skipOnError' => true, 'targetClass' => Additional::className(), 'targetAttribute' => ['additional_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['temporary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporaries::className(), 'targetAttribute' => ['temporary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказы',
            'additional_id' => 'Доп',
            'temporary_id' => 'Временно',
            'count' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditional()
    {
        return $this->hasOne(Additional::className(), ['id' => 'additional_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporary()
    {
        return $this->hasOne(Temporaries::className(), ['id' => 'temporary_id']);
    }
}
