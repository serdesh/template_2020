<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_additional}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%orders}}`
 * - `{{%additional}}`
 */
class m200321_163825_create_orders_additional_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_additional}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment("Заказы"),
            'additional_id' => $this->integer()->comment("Доп"),
            'temporary_id' => $this->integer()->comment("Временно"),
            'count' => $this->integer()->comment("Количество"),
        ]);

        // creates index for column `temporary_id`
        $this->createIndex(
            '{{%idx-orders_additional-temporary_id}}',
            '{{%orders_additional}}',
            'temporary_id'
        );

        // add foreign key for table `{{%temporaries}}`
        $this->addForeignKey(
            '{{%fk-orders_additional-temporary_id}}',
            '{{%orders_additional}}',
            'temporary_id',
            '{{%temporaries}}',
            'id',
            'CASCADE'
        );

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-orders_additional-order_id}}',
            '{{%orders_additional}}',
            'order_id'
        );

        // add foreign key for table `{{%orders}}`
        $this->addForeignKey(
            '{{%fk-orders_additional-order_id}}',
            '{{%orders_additional}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `additional_id`
        $this->createIndex(
            '{{%idx-orders_additional-additional_id}}',
            '{{%orders_additional}}',
            'additional_id'
        );

        // add foreign key for table `{{%additional}}`
        $this->addForeignKey(
            '{{%fk-orders_additional-additional_id}}',
            '{{%orders_additional}}',
            'additional_id',
            '{{%additional}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%temporaries}}`
        $this->dropForeignKey(
            '{{%fk-orders_additional-temporary_id}}',
            '{{%orders_additional}}'
        );

        // drops index for column `temporary_id`
        $this->dropIndex(
            '{{%idx-orders_additional-temporary_id}}',
            '{{%orders_additional}}'
        );
        
        // drops foreign key for table `{{%orders}}`
        $this->dropForeignKey(
            '{{%fk-orders_additional-order_id}}',
            '{{%orders_additional}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-orders_additional-order_id}}',
            '{{%orders_additional}}'
        );

        // drops foreign key for table `{{%additional}}`
        $this->dropForeignKey(
            '{{%fk-orders_additional-additional_id}}',
            '{{%orders_additional}}'
        );

        // drops index for column `additional_id`
        $this->dropIndex(
            '{{%idx-orders_additional-additional_id}}',
            '{{%orders_additional}}'
        );

        $this->dropTable('{{%orders_additional}}');
    }
}
