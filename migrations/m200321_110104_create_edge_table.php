<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%edge}}`.
 */
class m200321_110104_create_edge_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%edge}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment("Название"),
            'cost' => $this->float()->comment("Цена"),
        ]);

        Yii::$app->db->createCommand()->batchInsert('edge', 
            [ 'name', 'cost'], 
            [
                ['нет', '0'],
                ['0.4 мм', '50'],
                ['2 мм', '70'],
            ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%edge}}');
    }
}
