<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additional}}`.
 */
class m200321_113918_create_additional_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additional}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment("Название"),
            'cost' => $this->float()->comment("Цена"),
            'type_additional' => $this->string(255)->comment("Тип"),
        ]);

        Yii::$app->db->createCommand()->batchInsert('additional', 
            ['name', 'cost', 'type_additional'], 
            [
                ['Радиус 50-400 мм', '50', 'Эскизы'],
                ['Радиус 450-1000 мм', '150', 'Эскизы'],
                ['Прямоугольный выпил', '200', 'Эскизы'],
                ['Паз (м.п.)', '50', 'Фрезеровка'],
                ['Четверть (м.п.)', '100', 'Фрезеровка'],
                ['Наклонный пил (м.п.)', '200', 'Фрезеровка'],
                ['Криволинейный пил (шт.)', '250', 'Фрезеровка'],
                ['1 - 10 мм', '50', 'Присадка'],
                ['11 - 20 мм', '100', 'Присадка'],
                ['21 - 35 мм', '150', 'Присадка'],
            ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additional}}');
    }
}
