<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_ldsp}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%orders}}`
 * - `{{%ldsp}}`
 * - `{{%edge}}`
 * - `{{%edge}}`
 * - `{{%edge}}`
 * - `{{%edge}}`
 */
class m200321_130706_create_orders_ldsp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_ldsp}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment("Заказы"),
            'ldsp_id' => $this->integer()->comment("Ldsp"),
            'width' => $this->float()->comment("Длина"),
            'height' => $this->float()->comment("Ширина"),
            'count' => $this->integer()->comment("Количество"),
            'edge_width_left_id' => $this->integer()->comment("Кромка по длине"),
            'edge_width_right_id' => $this->integer()->comment("Кромка по длине"),
            'edge_height_left_id' => $this->integer()->comment("Кромка по ширине"),
            'edge_height_right_id' => $this->integer()->comment("Кромка по ширине"),
            'temporary_id' => $this->integer()->comment("Временно"),
            'comment' => $this->text()->comment("Примечание"),
            'file' => $this->string(255)->comment("Файл"),
        ]);

        // creates index for column `temporary_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-temporary_id}}',
            '{{%orders_ldsp}}',
            'temporary_id'
        );

        // add foreign key for table `{{%temporaries}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-temporary_id}}',
            '{{%orders_ldsp}}',
            'temporary_id',
            '{{%temporaries}}',
            'id',
            'CASCADE'
        );

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-order_id}}',
            '{{%orders_ldsp}}',
            'order_id'
        );

        // add foreign key for table `{{%orders}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-order_id}}',
            '{{%orders_ldsp}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ldsp_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-ldsp_id}}',
            '{{%orders_ldsp}}',
            'ldsp_id'
        );

        // add foreign key for table `{{%ldsp}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-ldsp_id}}',
            '{{%orders_ldsp}}',
            'ldsp_id',
            '{{%ldsp}}',
            'id',
            'CASCADE'
        );

        // creates index for column `edge_width_left_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-edge_width_left_id}}',
            '{{%orders_ldsp}}',
            'edge_width_left_id'
        );

        // add foreign key for table `{{%edge}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-edge_width_left_id}}',
            '{{%orders_ldsp}}',
            'edge_width_left_id',
            '{{%edge}}',
            'id',
            'CASCADE'
        );

        // creates index for column `edge_width_right_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-edge_width_right_id}}',
            '{{%orders_ldsp}}',
            'edge_width_right_id'
        );

        // add foreign key for table `{{%edge}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-edge_width_right_id}}',
            '{{%orders_ldsp}}',
            'edge_width_right_id',
            '{{%edge}}',
            'id',
            'CASCADE'
        );

        // creates index for column `edge_height_left_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-edge_height_left_id}}',
            '{{%orders_ldsp}}',
            'edge_height_left_id'
        );

        // add foreign key for table `{{%edge}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-edge_height_left_id}}',
            '{{%orders_ldsp}}',
            'edge_height_left_id',
            '{{%edge}}',
            'id',
            'CASCADE'
        );

        // creates index for column `edge_height_right_id`
        $this->createIndex(
            '{{%idx-orders_ldsp-edge_height_right_id}}',
            '{{%orders_ldsp}}',
            'edge_height_right_id'
        );

        // add foreign key for table `{{%edge}}`
        $this->addForeignKey(
            '{{%fk-orders_ldsp-edge_height_right_id}}',
            '{{%orders_ldsp}}',
            'edge_height_right_id',
            '{{%edge}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%temporaries}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-temporary_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `temporary_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-temporary_id}}',
            '{{%orders_ldsp}}'
        );
        
        // drops foreign key for table `{{%orders}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-order_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-order_id}}',
            '{{%orders_ldsp}}'
        );

        // drops foreign key for table `{{%ldsp}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-ldsp_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `ldsp_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-ldsp_id}}',
            '{{%orders_ldsp}}'
        );

        // drops foreign key for table `{{%edge}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-edge_width_left_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `edge_width_left_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-edge_width_left_id}}',
            '{{%orders_ldsp}}'
        );

        // drops foreign key for table `{{%edge}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-edge_width_right_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `edge_width_right_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-edge_width_right_id}}',
            '{{%orders_ldsp}}'
        );

        // drops foreign key for table `{{%edge}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-edge_height_left_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `edge_height_left_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-edge_height_left_id}}',
            '{{%orders_ldsp}}'
        );

        // drops foreign key for table `{{%edge}}`
        $this->dropForeignKey(
            '{{%fk-orders_ldsp-edge_height_right_id}}',
            '{{%orders_ldsp}}'
        );

        // drops index for column `edge_height_right_id`
        $this->dropIndex(
            '{{%idx-orders_ldsp-edge_height_right_id}}',
            '{{%orders_ldsp}}'
        );

        $this->dropTable('{{%orders_ldsp}}');
    }
}
