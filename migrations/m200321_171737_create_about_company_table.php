<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company}}`.
 */
class m200321_171737_create_about_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company}}', [
            'id' => $this->primaryKey(),
            'logo' => $this->string(255)->comment("Логотип"),
            'name' => $this->string(255)->comment("Наименования компании"),
            'address' => $this->text()->comment("Адрес"),
            'phone' => $this->string(255)->comment("Телефон"),
            'email' => $this->string(255)->comment("Email"),
            'site' => $this->string(255)->comment("Web Сайт"),
        ]);

        $this->insert('about_company',array(
            'name' => 'IT holding',
            'address' => 'Москва, ул. Ленина, 5',
            'email'=>'itholding@gmail.com',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%about_company}}');
    }
}
