<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersLdsp */
?>
<div class="orders-ldsp-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
