<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersLdsp */
?>
<div class="orders-ldsp-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'ldsp_id',
            'width',
            'height',
            'count',
            'edge_width_left_id',
            'edge_width_right_id',
            'edge_height_left_id',
            'edge_height_right_id',
            'comment:ntext',
        ],
    ]) ?>

</div>
