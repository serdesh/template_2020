<?php
use app\widgets\Alert;
use yii\widgets\Breadcrumbs;
?>          
	
    	<?php if($cont == 'calc') { ?>
    		<div style="padding: 0px 10px;">
    	<?php } else { ?>
    		<div id="content" class="content">
    	<?php } ?>
    	<div style="margin-top: -10px;margin-bottom: 5px;">
	    	<?= Breadcrumbs::widget([
		        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		    ]) ?>
		</div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>