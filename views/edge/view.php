<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Edge */
?>
<div class="edge-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'cost',
        ],
    ]) ?>

</div>
