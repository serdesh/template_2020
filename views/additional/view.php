<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Additional */
?>
<div class="additional-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'cost',
            'type_additional',
        ],
    ]) ?>

</div>
