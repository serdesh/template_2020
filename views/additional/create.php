<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Additional */

?>
<div class="additional-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
