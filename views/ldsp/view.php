<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ldsp */
?>
<div class="ldsp-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'cost',
            'thickness',
            'sorting',
            'type',
        ],
    ]) ?>

</div>
