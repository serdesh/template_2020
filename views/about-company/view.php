<?php
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

CrudAsset::register($this);
$this->title = "О компаниях";
$this->params['breadcrumbs'][] = $this->title;
$this->title = "О компаниях";
?>
    <div class="panel panel-inverse" data-sortable-id="ui-widget-14" style="">
        <div class="panel-heading">
            <div class="panel-heading-btn">

                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <!-- <h3 class="pane-title"> -->
                <a class="btn btn-sm btn-info"   role="modal-remote" href="<?=Url::toRoute(['update'])?>"><i class="fa fa-pencil"> Изменить</i> </a> 
            <!-- </h3> -->
        </div>
        <div class="panel-body">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                <div class="col-md-2">
                    <img src="<?= $model->getImage()?>" style = "width:150px; height:150px;object-fit: cover;">
                </div> 
                <div class="col-md-10 table-view" > 
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name',
                            'phone',
                            'site',
                            'address',
                            'email:email',

                        ],
                    ]) ?>
                </div>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="panel panel-inverse" data-sortable-id="ui-widget-14" style="">
        <div class="panel-heading">
            <div class="panel-heading-btn">

                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            Код виджета
        </div>
        <div class="panel-body">
            <h4>Код виджета</h4>
            <?=htmlspecialchars('<iframe width="100%" height="100%" id="otpwgt-undefined46756" src="https://raskroy.teo-crm.com/calc/" frameborder="0" style="width:100%; height: 100%" scrolling="yes" ></iframe>')?>
            
        </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

    