<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersAdditional */
?>
<div class="orders-additional-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
