<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersAdditional */
?>
<div class="orders-additional-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'additional_id',
            'count',
        ],
    ]) ?>

</div>
