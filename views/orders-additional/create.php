<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrdersAdditional */

?>
<div class="orders-additional-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
