<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\OrdersLdsp;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'№',
        'width' => '15px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ldsp_id',
        'label'=>'Цвет',
        'width'=>'200px',
        'content' => function($data){
            return $data->ldsp->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'width',
        'width'=>'85px',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->width,
                'id'=>'width'.$data->id,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style'=>'',
                    'onchange'=>" $.get('/orders-ldsp/set-values', {'id':$data->id, 'attribute': 'width', 'value':$(this).val()}, function(data){ 
                        document.getElementById('total_price').innerText = data;
                    } );  
                    $('#width{$data->id}').inputmask('decimal',{min:300,max:2780});",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'height',
        'width'=>'85px',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->height,
                'id'=>'height'.$data->id,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style'=>'',
                    'onchange'=>"$.get('/orders-ldsp/set-values', {'id':$data->id, 'attribute': 'height', 'value':$(this).val()}, function(data){ 
                        document.getElementById('total_price').innerText = data;
                    } ); 
                    $('#height{$data->id}').inputmask('decimal',{min:300 ,max:2050}); ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false,]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'width' => '30px',
        'format'=>'raw',
        'content' => function($data){
            return TouchSpin::widget([
                'name' => 'count'.$data->id,
                'value' => $data->count,
                'id' => 'count'.$data->id,
                'pluginOptions' => [
                    'width' => '30px',
                    'min' => 0, 'max' => 100000,
                    'buttonup_class' => 'btn btn-info', 
                    'buttondown_class' => 'btn btn-info', 
                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                    
                ],
                'options'=>[
                    'onchange'=>'
                    var a = $( "#count'.$data->id.'" ).val();
                    $.post( "/orders-ldsp/set-values?id='.$data->id.'&attribute=count&value="+a, function( data ){ document.getElementById("total_price").innerText = data; });
                        ' 
                ],
            ]); 
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'width' => '40px',
        'format'=>'html',
        'content' => function($data){
        return "<div style='width:30px; height:30px;'>
                 <input type='hidden' name='uname' id='uname{$data->id}' value='{$data->id}'>
                  <label for='inputFile_submit{$data->id}' class='btn'><i title='Загрузить файл' class='glyphicon glyphicon-circle-arrow-down' style='color:blue;margin-left:-5px;'></i></label>
                  <input id='inputFile_submit{$data->id}' name='file' style='visibility:hidden;' type='file'
                   onchange='uploadFileOrders({$data->id}); '>
                </div>".$data->downloadFile();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '200px',
        'format'=>'html',
        'content' => function($data){
            return "<input id='comment{$data->id}' type='text' value='{$data->comment}' class='form-control' 
                onchange=' var a = $(\"#comment{$data->id}\").val();
                $.post( \"/orders-ldsp/set-values?id={$data->id}&attribute=comment&value=\"+a, function( data ){ });
            '>";
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'width' => '15px',
        'header'=>'#',
        'template' => '{leadDelete}',
        'buttons'  => [
            'leadDelete' => function ($url, $model) {
                return "<i class='glyphicon glyphicon-trash text-danger' style='cursor:pointer;' title='Удалить'
                    onclick='
                        $.get( \"/orders-ldsp/delete?id={$model->id}\", function( data ){ 
                            document.getElementById(\"total_price\").innerText = data;
                            $.pjax.reload({container:\"#orgalit\", async: false});
                         });
                        $.pjax.reload({container:\"#orgalit\", async: false});
                        '
                ></i>";
            },
        ],
    ],

];   