<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\OrdersLdsp;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'№',
        'width' => '15px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ldsp_id',
        //'label'=>'(Толщина) Цвет',
        //'width'=>'150px',
        'content' => function($data){
            return /*'<b>('. $data->ldsp->thickness . ')</b> ' .*/ $data->ldsp->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ldsp_id',
        'label'=>'Толщина',
        'width'=>'50px',
        'content' => function($data){
            return $data->ldsp->thickness;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'width',
        'width'=>'70px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'height',
        'width'=>'70px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'label'=>'Кол-во',
        'width' => '30px',
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'edge_width_left_id',
        'width' => '150px',
        'format'=>'raw',
        'content' => function($data){
            return $data->edgeHeightLeft->name . ' - ' . $data->edgeHeightRight->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'edge_height_left_id',
        'width' => '150px',
        'format'=>'raw',
        'content' => function($data){
            return $data->edgeWidthLeft->name . ' - ' . $data->edgeWidthRight->name;
        }
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'width' => '40px',
        'format'=>'html',
        'content' => function($data){
        return $data->downloadFile();
        }
    ],
    
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '150px',
        'format'=>'html',
    ],
];   