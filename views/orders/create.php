<?php

use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\Ldsp;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;

$this->title = 'Калькулятор';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title">Выбор ЛДСП</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<label>Толщина</label>
				<?= kartik\select2\Select2::widget([
	                'name' => 'tolshina'.$model->id,
	                'id' => 'tolshina'.$model->id,
	                'data' => Ldsp::getThickness(),
	                'options' => [
	                    'placeholder' => 'Выберите',
	                    'onchange'=>'
                            $.post( "/orders-ldsp/ldsp-name?id='.'"+$(this).val(), function( data ){
                                $( "select#ldsp_name" ).html( data);
                            });' 
                        ], 
	                'size' => kartik\select2\Select2::SMALL,
	                'pluginOptions' => [ 
	                	'allowClear' => true
	                ],
            	]) ?>
			</div>
			<div class="col-md-4">
				<label>Цвет</label>
				<?= kartik\select2\Select2::widget([
	                'name' => 'ldsp'.$model->id,
	                'id' => 'ldsp_name',
	                'options' => [
	                    'placeholder' => 'Выберите',
	                ],
	                'size' => kartik\select2\Select2::SMALL,
	                'pluginOptions' => [ 
	                	'allowClear' => true
	                ],
	            ]) ?>
			</div>
			<div class="col-md-2">
				<label style="color:white;">.</label><br>
				<?= Html::button('Добавить деталь <i class="fa fa-plus"></i>',['title'=> 'Добавить деталь','class'=>'btn btn-warning','id'=>'create_ldsp']) ?>
			</div>
		</div>
		<br>
		<?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable']) ?>
		<div class="row">
			<div class="col-md-12 ">
					<?=GridView::widget([
		                'id'=>'crud-datatable',
		                'dataProvider' => $OrderLdsp,
		                //'filterModel' => $searchHistory,
		                'pjax'=>true,
		                'columns' => require(__DIR__.'/orders-ldsp/_columns.php'),
		                'striped' => true,
		                'summary' => false,
		                'condensed' => true,
		                'responsive' => true,
		            ])?>
			</div>
		</div>
		<?php Pjax::end() ?> 
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title">Оргалит</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label>Цвет</label>
				<?= Select2::widget([
	                'name' => 'orgalit' . $model->id,
	                'id' => 'orgalit_name' . $model->id,
	                'data' => Ldsp::getOrgalit(),
	                'options' => [
	                    'placeholder' => 'Выберите',
	                ],
	                'size' => Select2::SMALL,
	                'pluginOptions' => [ 
	                	'allowClear' => true
	                ],
	            ]) ?>
			</div>
			<div class="col-md-2">
				<label style="color:white;">.</label><br>
				<?= Html::button('Добавить деталь <i class="fa fa-plus"></i>',['title'=> 'Добавить деталь','class'=>'btn btn-warning','id'=>'orgalit_ldsp'.$model->id]) ?>
			</div>
		</div>
		<br>
		<?php Pjax::begin(['enablePushState' => false, 'id' => 'orgalit']) ?>
		<div class="row">
			<div class="col-md-12 ">
					<?=GridView::widget([
		                'id'=>'orgalit',
		                'dataProvider' => $OrderOrgalit,
		                //'filterModel' => $searchHistory,
		                'pjax'=>true,
		                'columns' => require(__DIR__.'/orders-ldsp/_columns_orgalit.php'),
		                'striped' => true,
		                'summary' => false,
		                'condensed' => true,
		                'responsive' => true,
		            ])?>
			</div>
		</div>
		<?php Pjax::end() ?> 
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title">Эскизы (общее количество)</h4>
	</div>
	<div class="panel-body bg-silver">
		<div class="row">	
			<?php foreach ($additionals as $value) { 
				if($value->additional->type_additional == 'Эскизы') {
			?>
				<div class="col-md-2">
					<?php 
						echo '<label class="control-label">' . $value->additional->name . '</label>';
						echo TouchSpin::widget([
						    'name' => 'ekskiz'.$value->id,
						    'value' => $value->count,
						    'id' => 'ekskiz'.$value->id,
						    'pluginOptions' => [
						        'buttonup_class' => 'btn btn-info', 
						        'buttondown_class' => 'btn btn-info', 
						        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
						        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
						        
						    ],
						    'options'=>[
						    	'onchange'=>'
						    	var a = $( "#ekskiz'.$value->id.'" ).val();
                                $.post( "/orders/set-values?temporary_id='.$model->id.'&additional_id='.$value->id.'&value="+a, function( data ){ document.getElementById("total_price").innerText = data; });
						    		' 
						    ],
						]);
					?>
				</div>
			<?php } } ?>
		</div>
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title">Фрезеровка (общее количество)</h4>
	</div>
	<div class="panel-body bg-silver">
		<div class="row">	
			<?php foreach ($additionals as $value) { 
				if($value->additional->type_additional == 'Фрезеровка') {
			?>
				<div class="col-md-2">
					<?php 
						echo '<label class="control-label">' . $value->additional->name . '</label>';
						echo TouchSpin::widget([
						    'name' => 'frezerovka'.$value->id,
						    'value' => $value->count,
						    'id' => 'frezerovka'.$value->id,
						    'pluginOptions' => [
						        'buttonup_class' => 'btn btn-info', 
						        'buttondown_class' => 'btn btn-info', 
						        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
						        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
						    ],
						    'options'=>[
							    'onchange'=>'
							    var a = $( "#frezerovka'.$value->id.'" ).val();
	                            $.post( "/orders/set-values?temporary_id='.$model->id.'&additional_id='.$value->id.'&value="+a, function( data ){ document.getElementById("total_price").innerText = data; });
							    	'
						    ],
						]);
					?>
				</div>
			<?php } } ?>
		</div>
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title">Присадка (общее количество)</h4>
	</div>
	<div class="panel-body bg-silver">
		<div class="row">	
			<?php foreach ($additionals as $value) { 
				if($value->additional->type_additional == 'Присадка') {
			?>
				<div class="col-md-2">
					<?php 
						echo '<label class="control-label">' . $value->additional->name . '</label>';
						echo TouchSpin::widget([
						    'name' => 'prisadka'.$value->id,
						    'value' => $value->count,
						    'id' => 'prisadka'.$value->id,
						    'pluginOptions' => [
						        'buttonup_class' => 'btn btn-info', 
						        'buttondown_class' => 'btn btn-info', 
						        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
						        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
						    ],
						    'options'=>[
							    'onchange'=>'
							    	var a = $( "#prisadka'.$value->id.'" ).val();
	                                $.post( "/orders/set-values?temporary_id='.$model->id.'&additional_id='.$value->id.'&value="+a, function( data ){
	                                	document.getElementById("total_price").innerText = data; });
							    	'
						    ],
						]);
					?>
				</div>
			<?php } } ?>
		</div>
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-body bg-warning">
		<span style="font-weight: bold; font-size: 18px;">Итоговая стоимость:</span> 
		<span style="margin-right: 20px; font-size: 18px; color:red;" id="total_price"><?=$model->price?> руб.</span>
		<?= Html::a('Отправить заявку <i class="fa fa-send"></i>', ['create-order', 'temporary_id' => $model->id], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-warning']) ?>
		<?= Html::a('Распечетать <i class="fa fa-print"></i>', ['print-temporary', 'temporary_id' => $model->id], ['role'=>'modal-remote', 'title'=> 'Распечетать', 'class'=>'btn btn-warning']) ?>
	</div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<script type="text/javascript" charset="utf-8" async defer>
	
		function uploadFileOrders(id){
			// alert(id);
		    var data = new FormData() ; 
		    var size = $( '#inputFile_submit'+id )[0].files[0].size;
		    if(size > 200 * 1024 * 1024){
		    	alert("файл слишком большой, максимальная(200 Mb)")
		    }else{
		    	data.append('file', $( '#inputFile_submit'+id )[0].files[0]) ; 
			    data.append('uname', $( '#uname'+id ).val()) ; 
			    $.ajax({
			    url: '/orders-ldsp/send-file',
			    type: 'POST',
			    data: data,
			    processData: false,
			    contentType: false,
			        success: function(data){ 
			        	 $.pjax.reload({container:'#crud-datatable', async: false});
			        	 $.pjax.reload({container:'#orgalit', async: false});
			       }
			    });
			    $.pjax.reload({container:'#crud-datatable', async: false});
			    $.pjax.reload({container:'#orgalit', async: false});
			    return false;
		    }
		    
		}
</script>

<script type="text/javascript" charset="utf-8" async defer>
	$(document).on('click', '#create_ldsp', function(e){
	        var ldsp = $( "#ldsp_name" ).val();
	        $.ajax({
	           type: 'POST',
	           data: {temporary_id: <?=$model->id?>, ldsp: ldsp },
	           url: '/orders-ldsp/set-ldsp',
	           success: function(data){ 
		        	$.pjax.reload({container:'#crud-datatable', async: false});
		        	$.pjax.reload({container:'#orgalit', async: false});
		       }
	        });
	        $.pjax.reload({container:'#crud-datatable', async: false});

	    });

</script>
<script type="text/javascript" charset="utf-8" async defer>
	$(document).on('click', '#orgalit_ldsp<?=$model->id?>', function(e){
	    var orgalit = $( "#orgalit_name<?=$model->id?>" ).val();
	    $.ajax({
	       type: 'POST',
	       data: {temporary_id: <?=$model->id?>, ldsp: orgalit },
	       url: '/orders-ldsp/set-ldsp',
	        success: function(data){ 
		        	$.pjax.reload({container:'#crud-datatable', async: false});
		        	$.pjax.reload({container:'#orgalit', async: false});
		       }
	    });
	    $.pjax.reload({container:'#orgalit', async: false});
	});

</script>